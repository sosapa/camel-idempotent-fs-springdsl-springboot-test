import ch.qos.logback.classic.encoder.PatternLayoutEncoder

import static ch.qos.logback.classic.Level.DEBUG

def LOGGING_PATTERN = "%date{ISO8601} [%thread] %-5level %logger{50} - %message%n"

// For Production set redrouter.logging.appenders = FILE,EMAIL.
def loggingAppenders = (System.getProperty("redrouter.logging.appenders", "CONSOLE")).split(",")*.trim()
def loggingPath = System.getProperty("redrouter.logging.path", System.getProperty("user.home") + "/test.log")

if ("CONSOLE" in loggingAppenders) {
  appender("CONSOLE", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
      pattern = LOGGING_PATTERN
    }
  }
}

if ("FILE" in loggingAppenders) {
  appender("FILE", RollingFileAppender) {
    encoder(PatternLayoutEncoder) {
      pattern = LOGGING_PATTERN
    }
    file = loggingPath
    rollingPolicy(TimeBasedRollingPolicy) {
      fileNamePattern = "${loggingPath}.%d{yyyy-MM-dd}"
    }
  }
}

root(DEBUG, loggingAppenders)
