// Copyright 2015 Red Energy
// John Hurst (john.hurst@gmail.com)
// 2015-05-21

package au.com.redenergy.camel.test

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.support.SpringBootServletInitializer
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.ImportResource

@SpringBootApplication
@Configuration
@ImportResource("classpath:/all-context.xml")
class Application extends SpringBootServletInitializer {

  static void main(String[] args) {
    SpringApplication.run(Application, args)
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    return builder.sources(Application)
  }

}
