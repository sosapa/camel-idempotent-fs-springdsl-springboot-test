# Apache Camel Test of idempotent file store, using spring DSL and spring boot

The release 2.20.1 seems to have changed the behaviour or have a bug with idempotent file stores. This is a simple project to verify that behaviour in isolation.

## Execution and assembly with
./gradlew clean bootRun

## Using
echo "content" > fromSource/test

## check 
The file should go to fileTarget
and also to file store

cat idempotent.file.store

## check2
echo "c0" > fromFolder/test~1

 wait till it finishes, then do

echo "c1" > fromFolder/test~1

check logs, should only be processed once